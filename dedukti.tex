% !TeX root = report.tex

\subsection{Dedukti}

Le partage de preuves est d'autant plus compliqué que le nombre de systèmes
de preuve augmente. En effet, il faut un traducteur de Coq à Lean,
de Lean à Coq, mais aussi de Coq à PVS, de PVS à Coq, etc. Le nombre
de traducteurs est quadratique !

En fait, le manque de standards se fait fortement ressentir ; il manque
une plateforme de partage, un système autour duquel le partage de preuves
s'articulerait. L'outil Dedukti développé par Deducteam se propose
pour cela.

Dedukti est une implémentation du $\lambda\Pi$-calcul Modulo Réécriture,
noté $\lambdapimod$, c'est-à-dire du $\lambda$-calcul avec des types dépendants
et des règles de réécritures (qui doivent être bien typées, ce qui est vérifié
par Dedukti). Par exemple, on définit les entiers naturels et l'addition ainsi.

\begin{minted}{coq}
Nat: Type.
zero: Nat.
succ: Nat -> Nat.
def plus: Nat -> Nat -> Nat.

[ n ] plus zero n --> n
[ n ] plus n zero --> n
[ n, m ] plus (succ n) m --> succ (plus n m)
[ n, m ] plus n (succ m) --> succ (plus n m).
\end{minted}

On définit quatre symboles \verb|Nat|, \verb|zero|, \verb|succ| et
\verb|plus|, et des règles de réécriture qui définissent \verb|plus|.

Notons que Dedukti n'est pas un assistant de preuve. Il s'agit d'un
\emph{framework logique} dans lequel nous allons \emph{encoder des théories}.
L'idée du partage de preuve d'un système $S_1$ de théorie $T_1$ vers un
système $S_2$ de théorie $T_2$ est la suivante.

\begin{enumerate}
    \item Nous encodons $T_1$ et $T_2$ dans Dedukti (notons les encodages
          $\Dedukti[T_i]$).
    \item Nous traduisons la preuve de $S_1$ dans $\Dedukti[T_1]$.
    \item Nous transformons cette traduction pour qu'elle soit dans
          $\Dedukti[T_2]$.
    \item Nous traduisons la preuve de $\Dedukti[T_2]$ dans $S_2$.
\end{enumerate}


Les étapes compliquées sont la première (encoder les théories) et
la troisième (passer d'un encodage à un autre).

Certains points de la première étape ont déjà été résolus. En effet, \cite{DowekCousineau} montre que le Calcul des Constructions et plus généralement tous les \emph{Pure Type System})
peuvent être encodés en $\lambdapimod$.

Les mathématiques inversées trouvent leur intérêt, vu ce que nous
voulons faire. Après avoir traduit de $T_1$ vers $\Dedukti[T_1]$, nous aimerions
supprimer certaines fonctionnalités pour pouvoir ensuite passer
facilement à $\Dedukti[T_2]$.

De plus, pour faciliter le passage de $\Dedukti[T_1]$ à $\Dedukti[T_2]$, ce
serait bien que les axiomes communs à $T_1$ et $T_2$ soient représentés
de la même manière dans les deux encodages.

L'idée de la traduction est schématisée en \cref{fig-dedukti}, avec en
\cref{fig-encodage} l'illustration d'encodages où les axiomes communs à
plusieurs théories sont représentées de la même manière ; dans ce cas, notre
but sera de transformer une preuve dans $\Dedukti[T_1]$ en une preuve
dans un sous-ensemble commun à l'encodage de plusieurs théories. L'idéal est
d'arriver dans le sous-ensemble commun aux théories (en bleu sur la figure), d'où l'intérêt des mathématiques inversées.

\begin{figure}[h]
    \centering
    \begin{subfigure}[b]{.6\linewidth}
        \begin{tikzpicture}
            \node [cloud,fill=red!20 ] (S1) {$S_1$};
            \node  [right = 1cm of S1,draw, ellipse, minimum width=0.5cm, minimum height = 1cm] (DT1) {$D[T_1]$};
            \node  [above right = 0.5cm and 0.5cm of DT1,draw, ellipse, minimum width=0.5cm, minimum height = 1cm] (DT2) {$D[T_2]$};
            \node  [below right = 0.5cm and 0.5cm of DT1,draw, ellipse, minimum width=0.5cm, minimum height = 1cm] (DT3) {$D[T_3]$};

            \node[cloud, color=blue!20, fit=(DT1) (DT3) (DT2), inner sep=-1mm, ellipse] (Dedukti) {};

            \node [cloud, right = 1cm of DT2, fill=white] (S2) {$S_2$};
            \node [cloud, right = 1cm of DT3, fill=white] (S3) {$S_3$};

            \path[->,thick, >=latex] (S1) edge node {} (DT1);
            \path[->,thick, >=latex] (DT1) edge node {} (DT2);
            \path[->,thick, >=latex] (DT1) edge node {} (DT3);
            \path[->,thick, >=latex] (DT2) edge node {} (S2);
            \path[->,thick, >=latex] (DT3) edge node {} (S3);
        \end{tikzpicture}
        \caption{Le partage de preuve.}
        \label{fig-partage}
    \end{subfigure}
    \begin{subfigure}[b]{.38\linewidth}
        \centering
        \def\firstcircle{ (0.0, 0.0) circle (1.2)} % 1.5
        \def\secondcircle{(2.0, 0.0) circle (1.2)}
        \def\thirdcircle{ (1.0,-1.5) circle (1.2)}

        \colorlet{circle edge}{black}
        \colorlet{circle area}{blue!30}

        \tikzset{filled/.style={fill=circle area, draw=circle edge, thick}}

        \begin{tikzpicture}
            \begin{scope}
                \clip  \firstcircle;
                \clip  \secondcircle;
                \fill[filled]   \thirdcircle ;
            \end{scope}
            \draw \firstcircle  node[left]  {$D[T_1]$};
            \draw \secondcircle node[right] {$D[T_2]$};
            \draw \thirdcircle  node[below] {$D[T_3]$};
        \end{tikzpicture}
        \caption{La forme d'encodage voulue.}
        \label{fig-encodage}
    \end{subfigure}
    \caption{Dedukti, plateforme d'échange.}
    \label{fig-dedukti}
\end{figure}

\begin{Question}
    Mais que veut-on dire par « encodage d'une théorie » ?
\end{Question}

Nous allons définir des symboles et des règles de réécriture
qui permettront d'écrire des termes du système à encoder.
Par exemple, voici un encodage des propositions, des preuves et de l'implication (\cf \cref{subsec-systeme} pour l'explication
de la règle de réécriture).

\begin{listing}[h]
\begin{minted}{coq}
Prop: Type.
imp: Prop -> Prop -> Prop.
Prf: Prop -> Type.

[p, q] Prf (imp p q) --> Prf p -> Prf q.
\end{minted}
\caption{Exemple d'encodage.}
\label{lst-encodage}
\end{listing}


\subsection{La théorie U}

Nous avons dit que si une fonctionnalité est présente dans $T_1$ et $T_2$,
alors ce serait bien que cette fonctionnalité soit encodée de la même
manière dans $\Dedukti[T_1]$ et dans $\Dedukti[T_2]$.

La théorie U, introduite dans \cite{theoryU}, est un très bon candidat pour cela. En effet, elle permet de représenter le Calcul des Constructions, et
l'encodage présenté dans l'article a la bonne propriété de permettre de
représenter facilement les sous-théories du Calcul des Constructions !

Pour cela, elle se base sur 38 axiomes (un axiome est un symbole de
Dedukti) qu'on peut combiner pour obtenir plusieurs propriétés. Par exemple,
on obtient la logique des prédicats minimale en ajoutant aux symboles et
règles définis en \cref{lst-encodage} ceux qui suivent.

\begin{minted}{coq}
Set: Type.
El: Set -> Type.
forall : x : Set -> (((El x) -> Prop) -> Prop).

[x,p] Prf (forall x p) --> z : El x -> (Prf (p z)).
\end{minted}

Avec \verb|El nat|, on aurait l'ensemble des éléments de \verb|nat|,
et $\forall$ prend en paramètre un terme de type \verb|Set| et une fonction de ce \verb|Set| vers \verb|Prop|, ce qui nous donne une nouvelle proposition.
Par exemple, nous pouvons écrire la proposition $\forall n \in \N, n > 0$
comme suit (le symbole \verb|=>| est la $\lambda$-abstraction de Dedukti).
\begin{minted}{coq}
forall nat (n: nat => positive n)
\end{minted}
Cela demande d'avoir défini le prédicat \verb|positive| et un terme \verb|nat| de type \verb|Set|.


\begin{Attention}[Une question d'encodages]
    Dans Dedukti, nous faisons bien des \emph{encodages}. Il serait possible
    d'avoir deux encodages différents d'une même théorie. La théorie U
    se présente comme une théorie « universelle » pour Dedukti.
\end{Attention}

Par exemple, la théorie des types simples $\STT$ est une sous-théorie du
Calcul des Constructions et est présente dans la théorie U
en prenant les axiomes correspondants à $\STT$. En fait, si nous avons
une preuve dans une sous-théorie $T$ de $\CoC$ (que ce soit $\STT$, $\CoC$
ou la logique des prédicats), nous pouvons l'exprimer dans la théorie
U en utilisant les axiomes de la théorie $U$ nécessaires pour définir la
théorie $T$.


\subsection{Logipedia, un premier pas sur la Terre commune}

\bsc{François THIRÉ} a déjà fait du partage de preuves. Pour cela, il a introduit une logique, $\STTFA$, correspondant à la théorie des types simples
avec du polymorphisme et des opérateurs de types.
Logipedia, un programme permettant d'exporter des preuves de $\Dedukti[\STTFA]$
vers plusieurs systèmes de preuves (Coq, HOL Light, Lean, Matita, Open Theory
et PVS) a été écrit, ce qui a permis la traduction d'une bibliothèque d'arithmétique contenant une preuve du petit théorème de \bsc{FERMAT} (\cf{} \cite{Thire, theseThire}).

Ce travail n'est pas basé sur la théorie U, mais montre que le
partage de preuves peut bien se faire !
