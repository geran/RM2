% !TeX root = report.tex


\section{GeoCoq, de Coq aux autres systèmes}

Mon travail était de partager GeoCoq
\footnote{\url{https://geocoq.github.io/GeoCoq/}}, une bibliothèque Coq de
géométrie euclidienne, et d'ainsi obtenir une formalisation du Livre I des
Éléments d'\bsc{Euclide} (une partie de GeoCoq) dans d'autres systèmes de
preuves.

Nous partons donc d'une preuve Coq, donc dans le Calcul des Constructions
avec univers et types inductifs, et nous voulons l'encoder en Dedukti,
la transformer pour l'avoir dans une logique très simple, si possible
dans la théorie U, et l'exporter vers d'autres systèmes.

Il semble raisonnable de se dire que si une preuve n'utilise que des
opérations sur les entiers et ne quantifie jamais sur des univers, alors
il n'y a aucune raison que des univers tels que $\Type_3$ ou encore du
polymorphisme y apparaissent. De même, le livre I des Éléments d'\verb|Euclide|
devrait pouvoir se faire sans ces concepts et est donc un bon test pour le
partage de preuve.

\subsection{Vodk, de Coq à Dedukti}

Beaucoup de personnes ont travaillé sur un encodage de Coq en Dedukti, et
un traducteur de Coq vers Dedukti, Vodk\footnote{\url{https://github.com/Deducteam/CoqInE}}, a été écrit. Dans \cite{Burel},
\bsc{Mathieu Boespflug} et \bsc{Guillaume Burel} donnent notamment un
encodage des types inductifs et \bsc{Gaspard Férey} montre comment gérer la
les univers et leur cumulativité dans \cite{theseFerey}.
Je donne rapidement ici quelques points qui montrent intuitivement comment encoder les univers.

Le gros problème est qu'en Coq, un objet a plusieurs types (s'il est
dans l'univers $\Type_i$, alors il est dans l'univers $\Type_j$ pour tous
$j > i$), alors qu'en Dedukti, un objet a un seul type. Ainsi, si une
fonction attend un élément de $\Type_j$ et que nous voulons lui passer
un élément de $\Type_i$ avec $j > i$, ce sera possible en Coq et nous
voulons rendre cela possible en Dedukti.

L'idée est d'avoir un opérateur \verb|cast|, permettant de « traverser
les univers ». Si $t$ a le type $\Type_i$, alors \verb|cast| permet de
construire un objet de type $\Type_j$ pour tout $j$ supérieur à $i$ ; on
a promu $t$ !

Pour que cela reste correct, le \verb|cast| demande une preuve de sous-typage
(donc que $j$ est bien supérieur à $i$) et cette preuve est faite à l'aide de
règles de réécritures (on sait que $t$ a le type $\Type_i$ et qu'on veut le
transformer en $\Type_j$, donc on a accès à $i$ et à $j$).

Donc, Vodk permet d'obtenir une preuve de Coq dans un encodage $\Dedukti[\Coq]$
où est notamment présent cet opérateur \verb|cast|.

\subsection{Au contact de Vodk}

En utilisant Vodk, j'ai pu traduire GeoCoq dans l'encodage $\Dedukti[\Coq]$ de
\bsc{Gaspard Férey} et commencer mon travail de minimisation qui se déroule
en plusieurs étapes.
\begin{enumerate}
    \item Supprimer les casts.
    \item Utiliser les connecteurs de la théorie U.
    \item Supprimer des égalités entre propositions.
\end{enumerate}
Je me suis particulièrement servi de DKMeta
\footnote{\url{https://github.com/Deducteam/dkmeta}},
un outil de réécriture de termes Dedukti. Il permet, étant donné des règles de réécriture (appelées
règles de méta-réécriture) et des fichiers Dedukti, de réécrire
les fichiers Dedukti en appliquant les règles de méta-réécriture.
Une première simplification (qui n'est pas dans les étapes décrites plus haut)
s'est faite au niveau des fichiers Coq. En effet, les axiomes d'Euclide étaient
définis dans une classe qu'il fallait traduire, ce qui
complexifiait la traduction obtenue.

Placer les axiomes en dehors de la classe et la supprimer nous offrait déjà
une certaine simplification : dans la traduction de la version avec classe,
chaque fonction prenait en paramètre chaque élément de la classe, alors que
ce n'était pas nécessaire. Par exemple, on passe de quarante à trois lignes
pour la définition de l'égalité entre deux points.

\begin{minted}{coq}
def eqpoint :
  x:(C.T C.Type Point) -> y:(C.T C.Type Point) -> C.U prop
:= Coq_Logic.eq Point.
\end{minted}

Ici, \verb|C.U univers| encode l'univers \verb|univers| et
\verb|C.T | correspond à l'encodage des termes, donc
\verb|C.T C.Type Point| correspond au types des points.

Mon travail est disponible sur \href{https://github.com/Karnaj/DGCE}{mon Github} \footnote{\url{https://github.com/Karnaj/DGCE}}

\subsection{Simplification des univers}

La première chose que l'on remarque, c'est l'omniprésence de \verb|cast|.
La première étape de mon travail a donc été de chercher à les éliminer, les Éléments d'\verb|Euclide| ne semblant pas nécessiter tous ces univers.

\begin{Question}
    Pourquoi ces casts apparaissent-ils ?
\end{Question}

La plupart (quasiment tous) ces casts apparaissent parce que lors de la
traduction (donc dans Vodk), on fait un cast dès qu'une fonction demande
un élément d'un univers particulier. Mais on se rend compte que la majorité
des \verb|cast| sont inutiles et vont d'un univers au même univers. On a alors trois types de casts.

\subsubsection{Des casts triviaux}

\begin{minted}{coq}
C.cast C.Type1 C.Type1 C.u0 C.u0 C.I C.prop
C.cast C.prop C.prop prop_ex prop_ex C.I prop_ex_proof
\end{minted}

Il s'agit des \verb|cast| qui vont d'un univers dans le même univers.
Ici, le second cast par exemple nous transforme \verb|prop_ex_proof| qui
est un élément de \verb|prop_ex| (comme dit en \cref{subsec-systeme}, une
proposition est un type dont les éléments sont ses preuves). Avec DKMeta,
j'ai pu réécrire ces casts (en \verb|prop_ex_proof| dans le deuxième cas).

\subsubsection{Des casts avec le même type « caché »}

Je me suis alors rendu compte que \verb|cast| permettait de transformer
des preuves d'une proposition $P$ en preuve de $P'$, mais où $P$ et $P'$
sont la même expression. Voici la forme générale de ces casts et un exemple.

\begin{minted}{coq}
C.cast C.prop C.prop prop_ex1 prop_ex2 C.I prop_ex1_proof

C.cast (C.rule C.prop C.prop) C.prop
       (C.prod C.prop C.prop (C.rule C.prop C.prop)
               C.I P (H: P => Coq__Init__Logic.False))
       (Coq__Init__Logic.not P)
       C.I
       proof
\end{minted}

Ce type de cast est souvent présent avec comme propositions une fonction
,d'un côté, et sa définition de l'autre : ici, notre exemple permet de
transformer une preuve de \verb|P => False| (la définition de \verb|not P|) en
une preuve de \verb|not P|.

Cet exemple me permet d'introduire \verb|C.prod|, l'encodage du produit
de Coq. Il prend en paramètre trois univers (celui de l'élément d'entrée,
celui de l'élément d'arrivé, et celui de l'abstraction ainsi formée),
une preuve qu'on peut bien construire un produit de cette manière, un élément
de l'univers de départ et la fonction correspondant). L'univers d'un produit
est « calculé » grâce à \verb|C.rule| et de la réécriture :
une règle permet de réécrire \verb|C.rule s1 s2| en l'univers des abstractions de \verb|s1|
dans \verb|s2|.

Par exemple, une règle réécrit \verb|C.rule C.prop C.prop|en \verb|C.prop|,
c'est-à-dire qu'ici, on a une fonction de \verb|C.prop| vers \verb|C.prop| et
qu'une telle fonction est un \verb|C.prop|. En fait une telle fonction correspond à une implication, et on est bien en train de caster une preuve
d'implication.

J'ai utilisé DKMeta pour simplifier les \verb|rule|, puis pour supprimer ces \verb|cast|.

\begin{Information}
    Cet exemple nous montre que Vodk traduit les implications
    comme des produits et que lorsqu'on veut se servir d'une implication
    comme d'une proposition, on fait un cast inutile qui nous dit
    qu'un produit de \verb|Prop| vers \verb|Prop| peut être « converti »
    en \verb|Prop|, c'est-à-dire qu'une implication est une proposition.

    Notons alors que nous voulons également supprimer le plus de produits
    possibles et ne garder que les implications !
\end{Information}

D'autres casts similaires étaient présents, par exemple pour transformer
une implication (sous forme de produit) en proposition et pouvaient être
supprimés car une preuve de \verb|P => Q| \emph{est} une fonction (dans
le contexte où ces casts apparaissaient, c'est la preuve de la proposition
et pas la proposition qui était utilisé). En fait, même le produit \verb|prod|
pouvait alors être supprimé !

\subsubsection{Des problèmes de principe inductif}

Certains \verb|cast| étaient dûs aux principes inductifs de l'égalité
et de certains connecteurs. Par exemple, pour l'égalité, nous avons
\verb|eq_rect| et \verb|eq_ind|.

\begin{minted}{coq}
def eq__rect :
A:(C.U Type) -> x:(C.T Type A) ->
P:(__:(C.T Type A) -> C.U Type) ->
f:(C.T C.Type (P x)) -> y:(C.T C.Type A) ->
e:(C.T C.prop (Coq_Logic.eq A x y)) -> C.T C.Type (P y) := ...

def eq__ind :
A:(C.U Type) -> x:(C.T Type A) ->
P:(__:(C.T Type A) -> C.U C.prop) ->
f:(C.T C.prop (P x)) -> y:(C.T Type A) ->
e:(C.T C.prop (Coq_Logic.eq A x y)) -> C.T C.prop (P y) := ...
\end{minted}

On donne un type $A$, un élément $x$ de ce type, un prédicat $P$,
une preuve de $P(x)$, un élément $y$ de $A$, une preuve que $x = y$, et
on obtient une preuve de $P(y)$. La différence entre les deux : \verb|eq_rect|
attend un prédicat qui renvoie un \verb|Type|, \verb|eq_ind| attend un
« vrai » prédicat, donc qui renvoie une proposition.

On a de même \verb|and__ind| et \verb|and__rect|, etc. Seules les versions
en \verb|ind| sont nécessaires pour ce que l'on veut faire. Et pourtant, il
se trouve que beaucoup de versions avec \verb|rect| apparaissent dans nos
preuves... Avec des \verb|cast| pour transformer les prédicats (qui renvoient
toujours des propositions), en des prédicats qui renvoient des \verb|Type|.

Encore une fois, on peut simplifier cela et faire disparaître toutes
les versions avec \verb|rect| à l'aide de règles de méta-réécriture.

Finalement, à cette étape, on se rend compte que les seuls types dont
on a besoin pour exprimer nos preuves sont les propositions, les points,
et les cercles ! Puisqu'aucun autre type n'apparaît, on peut se contenter
de deux univers $U_0$ et $U_1$. L'univers $U_1$ est celui des types
(on retrouve \verb|Prop|, \verb|Circle| et \verb|Point| dans cet univers),
et $U_0$ correspond à l'univers des propositions \verb|Prop|.


\subsection{Vers la théorie U ?} \label{subsec-vers-U}

Seul restait un dernier petit bastion d'univers et je pourrais alors essayer
d'avoir les preuves dans l'encodage de la théorie U ! Ce dernier bastion était
lié à la traduction des fichiers de Coq liés à la logique (dont tout ce qui
définissait les connecteurs et l'égalité).

Il s'agissait d'une petite trentaine de définitions (les connecteurs, leurs
règles d'élimination et d'introduction), etc. Et pour supprimer ces derniers
restes d'univers, et se rapprocher de la théorie U, j'ai décidé
de tout simplement les remplacer par les connecteurs de la théorie U !

Le point important ici est que ce qui nous intéresse avec un connecteur, ce
n'est pas la manière dont il est défini, mais comment on en construit une preuve et comment on utilise cette preuve ! Dans la traduction de Vodk, le connecteur \verb|and| était défini ainsi.

\begin{minted}{coq}
and: A: C.U C.prop -> B: C.U C.prop -> C.U C.prop.

conj: A: C.U C.prop -> B: C.U C.prop ->
      H1: C.T C.prop A -> H2: C.T C.prop B ->
      C.T C.prop (and A B).
\end{minted}

Et on l'utilisait à travers ces deux fonctions.

\begin{minted}{coq}
def match____and:
  s: C.S -> A: C.U C.prop -> B: C.U C.prop ->
  P: (C.T C.prop (and A B) -> C.U s) ->
  case__conj: (H1: C.T C.prop A -> H2: C.T C.prop B ->
    C.T s (P (conj A B H1 H2))) ->
  H: C.T C.prop (and A B) -> C.T s (P H).

def and__ind:
  A: C.U C.prop -> B: C.U C.prop -> P: C.U C.prop ->
  f: (H1: C.T C.prop A -> H2: C.T C.prop B -> C.T C.prop P) ->
  H: C.T C.prop (and A B) -> C.T C.prop P := ...
\end{minted}

\verb|and__ind| est construite à partir de \verb|match____and|,
mais les deux nous permettent d'utiliser une preuve de \verb|and A B|.
Si on a une fonction qui à partir d'une preuve de  \verb|and A B| nous donne un
élément dans l'univers \verb|s| et qu'on a une preuve de  \verb|and A B|, alors
on saura obtenir cet élément de \verb|s|. La version  \verb|and__ind| est plus restrictive ; elle ne permet de montrer que des propositions.

En fait, seul \verb|and__ind| est nécessaire ; on ne veut montrer que des
propositions. Cela signifie que pour supprimer le \verb|and| traduit par
\verb|vodk|, il nous suffit de savoir exprimer \verb|and__ind| et \verb|conj| à
partir du \verb|and| de la théorie U, ce qui est fait ci-dessous !

\begin{minted}{coq}
and: Prop -> Prop -> Prop.

[p2, p1] Prf (and p1 p2) -->
p:Prop -> ((Prf p1) -> (Prf p2) -> Prf p) -> Prf p.

def conj:
  A:Prop -> B:Prop -> H1:(Prf A) -> H2:(Prf B) ->
  Prf (and A B) :=
  A:Prop => B:Prop => H1:(Prf A) => H2:(Prf B) =>
  p:Prop => f:((Prf A) -> (Prf B) -> Prf p) => f H1 H2.

def and__ind:
  A:Prop -> B:Prop -> P:Prop ->
  f:(H1:(Prf A) -> H2:(Prf B) -> Prf P) ->
  H:(Prf (and A B)) ->
  Prf P :=
  A:Prop => B:Prop => P:Prop =>
  f:(H1:(Prf A) -> H2:(Prf B) -> Prf P) =>
  H:(Prf (and A B)) =>
  H P f.
\end{minted}

On utilise le fait qu'une preuve de $A \land B$ est, par la règle
de réécriture donnée, une fonction qui, étant donné une proposition $P$
et une preuve de $A \to B \to P$, construit une preuve de $P$.

\begin{Information}
    Les preuves du Livre I des Éléments d'Euclide n'utilisaient
    pas de type inductif autre que celui des booléens (que nous
    venons de remplacer en utilisant les éléments
    de logique de la théorie U).
\end{Information}

Notons que j'ai profité de cette étape de ma traduction pour transformer mes
preuves en utilisant les notations de la théorie U. En particulier, j'ai
totalement supprimé tout ce qui me restait comme référence aux univers.

\begin{itemize}
    \item \verb|C.U C.prop| devient \verb|C.Prop| (l'univers des proposition).
    \item \verb|C.T C.prop P| devient \verb|C.Prf P| (un terme de \verb|P|
    est une preuve de \verb|P|).
    \item \verb|C.T C._0 Circle| devient \verb|C.El Circle|.
\end{itemize}

Et ça y est, nous n'avons plus aucun univers dans notre traduction !
Nous sommes donc, au pire des cas, dans le calcul des constructions, donc
dans la théorie U !

À cette étape de la transformation, il me reste quelques produits et
certains d'entre eux correspondent à des des implications ; je les ai
alors remplacé par l'implication de la théorie U.

Ces produits apparaissaient par exemple avec le principe inductif
de l'égalité qui prend en paramètre un prédicat qui peut
être de la forme \verb|X: Point => C.prod P Q| . Dans ce contexte, on veut
un prédicat de \verb|Point| vers \verb|Prop| (et ce n'est pas la preuve
de l'implication qui nous intéresse, mais bien l'implication), et donc
nous ne pouvons pas supprimer le produit comme nous l'avons fait précédemment.

Ça y est, nous sommes, à quelques détails près dans l'encodage présenté
de la théorie U. Le plus gros détail se situe au niveau des types
des fonctions. Voici à quoi ressemblaient les types de mes objets à ce moment.

\begin{minted}{coq}
thm lemma__congruencesymmetric :
  A:(C.El Point) -> B:(C.El Point) ->
  C:(C.El Point) -> D:(C.El Point) ->
  H:(C.Prf (Axioms__euclidean__axioms.Cong B C A D)) ->
  C.Prf (Axioms__euclidean__axioms.Cong A D B C) := ...
\end{minted}

Dans la théorie U, on voudrait plutôt ce genre de type.

\begin{minted}{coq}
thm lemma__congruencesymmetric :
C.Prf (
  C.forall Point (A:(C.El Point) =>
    C.forall Point (B:(C.El Point) =>
      C.forall Point (C:(C.El Point) =>
        C.forall Point (D:(C.El Point) =>
          C.imp (Axioms__euclidean__axioms.Cong B C A D)
                (Axioms__euclidean__axioms.Cong A D B C)))))) := ...
\end{minted}

La notation précédente est moins verbeuse, mais elle a le désavantage
de ne pas être dans l'encodage. Avec cette dernière notation, nous
sommes certains d'être bien dans l'encodage !

Nous voudrions faire ce remplacement avec DKMeta en utilisant une
règle de ce genre.

\begin{minted}{coq}
[T, Q] prod.prod (C.El T) (x => C.Prf (Q x)) -->
C.Prf (C.forall T (x: C.El T => (Q x))).
\end{minted}

Qui permettrait de transformer
\verb|A:(C.El Point) -> C.Prf True.| en ce type.

\begin{minted}{coq}
C.Prf (C.forall Point (x: C.El Point => True)).
\end{minted}

Mais en fait, on obtient le type suivant.

\begin{minted}{coq}
C.Prf (C.forall Point (x: C.El Point => (x: C.El Point => True) x).
\end{minted}

C'est le même type, mais qui n'est pas bêta-réduit. En fait,
DKMeta fait la bêta-réduction par défaut, mais si elle le fait,
elle le fait pour tous les termes, ce que je ne voulais car
cela faisait beaucoup augmenter la taille de certains
termes et le temps qu'ils mettaient à être vérifiés.

Je ne pouvais donc pas utiliser DKMeta pour transformer mes types
et j'ai finalement choisi de garder la première forme de type.

\subsection{En logique des prédicats ?}

Reste à savoir dans quel fragment de la théorie U sont les preuves à
cette étape. Elles semblent être en théorie des types simples voire même en
logique des prédicats, mais quelques petits points réfutent cette
hypothèse.

En effet, les preuves contiennent des égalités de propositions,
ce qu'il n'y a pas en logique des prédicats. Ce problème n'est pas,
en théorie, compliqué à régler. Une égalité de deux propositions,
c'est une équivalence ! Néanmoins, on peut se demander pourquoi de
telles égalités sont présentes. J'ai conjecturé que c'est dû à
certaines tactiques de Coq qui font qu'on se retrouve
dans la situation suivante. On veut montrer que \verb|P(c)|,
sachant que \verb|P(d)| est vraie.

\begin{itemize}
    \item \verb|f| renvoie une preuve que \verb|P(d) = P(c)|. Pour
    cela, elle utilise une preuve que \verb|d = c| construite par \verb|h|.
    \item \verb|g| utilise la preuve que \verb|P(d) = P(c)| avec \verb|eq__ind|
    pour obtenir une preuve de \verb|P(c)|.
\end{itemize}
On se rend bien compte que c'est absurde et qu'on peut directement construire
une preuve de \verb|P(c)| si on sait construire une preuve de \verb|d = c| et
qu'on a une preuve de \verb|P(d)|. Je transforme alors la preuve en faisant
prendre en paramètre à \verb|f| une preuve de \verb|P(d)| et renvoyer
directement une preuve de \verb|P(c)|.

Pour automatiser cette transformation, j'ai écrit un outil qui détecte les
fonctions \verb|f| qui renvoient des preuves de \verb|P = Q|, je les modifie
pour qu'elles prennent en paramètre une preuve de \verb|P| et renvoient une
preuve de \verb|Q|, et il faut ensuite chercher les appels à cette fonction et
les modifier. J'ai écrit un outil qui se charge de cela. Dans le cas
de GeoCoq, il les remplace tous, mais en théorie, il me faudrait gérer
plus de cas pour qu'il soit « universel », ce que je n'ai pas encore fait.

De plus, les preuves utilisent du polymorphisme (et donc on n'est même pas en
théorie des types simples). En effet, l'égalité ne fait pas partie de la théorie U, et pour la définir , nous utilisons
du polymorphisme (nous voulons une égalité pour les points, les cercles, etc.).
Ce point s'arrange de deux manières.
\begin{itemize}
    \item Créer deux égalités \verb|eqPoint| et \verb|eqCircle| et
          dupliquer chaque fonction de l'égalité pour avoir \verb|eqPoint__ind|
          et \verb|eqCircle__ind| (on instancie directement chaque fonction
          liés à l'égalité avec le type voulu).
    \item Rajouter l'égalité (polymorphe) à la théorie U (et
    avoir une théorie $U_=$).
\end{itemize}
J'ai choisi la deuxième option ; il ne semble pas aberrant de
considérer l'égalité comme un objet basique de la théorie
\footnote{Depuis, l'égalité a été rajoutée à la théorie U}.

\begin{comment}
D'ailleurs, d'autres fonctions polymorphes existent, toujours du côté
des fichiers de logique de nos preuves. Par exemple, \verb|exists__ind|
demande logiquement le type d'éléments sur lequel on travaille (le
\verb|A| du code qui suit).

\begin{minted}{coq}
def ex__ind :
A:C.Set ->
P:(__:(C.El A) -> C.Prop) ->
P0:(C.Prop) ->
f:(x:(C.El A) -> __:(C.Prf (P x)) -> C.Prf P0) ->
e:(C.Prf (C.exists A P)) -> C.Prf P0
\end{minted}

De même, certaines fonctions prennent en paramètre un prédicat, ce qui
demanderait la fonctionnalité (les types comme \verb|set1 -> Prop|) et nous oblige à rester en théorie des types simples. Mais là-encore, ces fonctions sont liés à la
logique (par exemple \verb|ex__ind| ci-dessus prend en paramètre un
prédicat), et on ne va pas en tenir rigueur.

\begin{Information}
Il pourrait être intéressant de créer un type \verb|Predicate|
et de l'utiliser dans ces fonctions pour mettre en exergue ce point.
\end{Information}
\end{comment}

\subsection{De la simplification}

J'en ai également profité pour simplifier quelques preuves. Ces simplifications
ne produisaient pas des termes dans une théorie plus petite, mais elles
diminuaient leur taille, ce qui est toujours appréciable.
Par exemple, des \verb|eq_trans| inutiles étaient utilisées (pour prouver $A = B$, on montre $A = A$ et $A = B$) ou encore pour montrer $P(x)$ sachant
que $P(y)$ est vrai et que $x = y$, on utilise \verb|eq_ind| avec
le prédicat
\[
    z \mapsto H_1 \implies \cdots \implies H_n \implies P(z)
    \implies H_{n + 1} \implies \cdots \implies H_{m}
\]
au lieu d'utiliser le prédicat $z \mapsto P(z)$. J'ai écrit un outil qui
fait ces simplifications.

Une autre simplification que je n'ai pas totalement faite consiste à
supprimer les arguments inutiles de certaines fonctions (des hypothèses
inutiles ou encore des points et des cercles non utilisés dans le corps
de la fonction). En effet, il n'est pas rare de voir dans le code des
fonctions avec des dizaines, voire des centaines d'arguments (une
simplification de ce type a permis d'enlever 3000 lignes à plusieurs
fonctions).

\begin{Attention}
Bien sûr, je ne touche pas aux propositions et aux théorèmes du livre
d'Euclide. Sur ce point je ne modifie que les lemmes introduits par
Vodk lors de la traduction.
\end{Attention}

Néanmoins, ce n'est pas si simple à faire. Il faut par exemple
se demander comment gérer les cas des fonctions partielles ou encore
les $x \mapsto g(x, c)$ où $g(x, c)  = f(a, x, c, d)$ et où $f(a, b, c, d)$
n'utilise pas $b$.

\subsection{Vers \STTFA{} et vers les autres systèmes}

Pour obtenir les preuves dans les autres systèmes, j'ai décidé de passer
par \STTFA{} et Logipedia. J'avais ma preuve en logique des prédicats, donc
il ne devait pas y avoir de gros problèmes pour utiliser \STTFA{} qui est,
rappelons-le, de la théorie des types simples avec polymorphisme et
opérateurs de types (et donc une extension de la logique des prédicats).
En fait, il y a eu deux soucis majeurs.

\subsubsection{Les connecteurs}

Dans \STTFA{}, les seuls connecteurs sont l'implication et le quatificateur
universel. J'ai donc dû définir les autres connecteurs dans \STTFA{}. Le
problème est que, puisque je dois rester dans \STTFA{}, je dois passer
d'un symbole et de règles de réécriture à une définition.

Pour ce point, il faut garder en tête que ce qui nous intéresse,
c'est comment se comporte une preuve de la proposition $A \land B$
par exemple, comportement donné par la règle de réécriture dans
la théorie U. Ceci permet de savoir comment définir le connecteur
dans \STTFA{}.

\begin{minted}{coq}
(; dans la théorie U ;) [p1,p2] Prf (and p1 p2) -->
p: Prop -> (Prf p1 -> Prf p2 -> Prf p) -> Prf p.

(; dans STTFA ;) def and :
etap (p arrow bool (arrow bool bool))
:=
  x:(etap (p bool)) =>
  y :(etap (p bool)) =>
  forall bool
(z: (etap (p bool)) =>
    impl (impl x (impl y z)) z).
\end{minted}

Ici, \verb|etap (p type_ex)| correspond à un élément du type \verb|type_ex|.
On définit $x \land y$ comme la fonction qui à $x$ et $y$ associe la
proposition $\forall y, (x \implies y \implies z) \implies z$ et le fait
qu'une preuve de $P \implies Q$ soit une fonction des preuves de $P$ dans les
preuves de $Q$ assure qu'une preuve de $x \land y$ dans \STTFA{} se comporte
de la même manière qu'une preuve de $x \land y$ dans la théorie U.

\subsubsection{Être \emph{vraiment} dans \STTFA}

Logipedia ne traduit que des preuves qui sont \emph{vraiment} dans son
encodage de \STTFA{}. On retombe alors sur le problème de types soulevé
en \cref{subsec-vers-U}.

\begin{minted}{coq}
eta Point -> eta bool : KO.
eta arrow Point bool  : OK.
\end{minted}

J'ai écrit un outil, basé sur des expressions régulières, pour faire la
transformation. Ici, je ne travaille donc pas sur l'AST des preuves ,
mais sur le texte.

Et j'ai enfin pu traduire les fichiers vers les autres systèmes de preuve.
Je donne dans le tableau \cref{tab-bench} les temps de traduction et les tailles
des preuves générées. Pour comparaison, la première traduction
en Dedukti faisait environ 500 Mo, celle obtenue en logique des
prédicats environ 250 Mo, et celle en \STTFA environ 300 Mo.

\begin{table}[h]
    \centering
    \renewcommand{\arraystretch}{1.4}
    \begin{tabular}{@{}lcccccc@{}}
        \toprule
        & Coq       & HOL Light & Lean     & Matita  & Open Theory & PVS  \\
        \midrule
Taille  & 100 Mo    & 250 Mo    & 150 Mo   & 100 Mo  & 1.9 Go      & 1.3 Go\\
Durée   & 13h50     & 14h10     & 13h20    & 12h10   & 14h40       & 15h20h\\
    \bottomrule
    \end{tabular}
    \caption{\etranger{Benchmark} des différentes traductions.}
    \label{tab-bench}
\end{table}


