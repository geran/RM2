% !TeX root = report.tex

\section{Les mathématiques inversées et le partage de preuves}

\subsection{Les systèmes de preuve}
\label{subsec-systeme}
Les assistants de preuve nous permettent d'utiliser les capacités des
machines à manipuler rapidement de grosses expressions pour formaliser
des raisonnements. Par exemple, le théorème des quatre couleurs, prouvé
avec un assistant de preuve, a demandé l'étude d'un peu plus
de mille cas !

De plus, il est plus facile de croire en une preuve vérifiée informatiquement,
qu'en une preuve énoncée et écrite « normalement ». Ainsi, avoir une preuve
formalisée dans un assistant de preuve permet également d'en être plus sûr.

Néanmoins, il faut « faire confiance à l'assistant de preuves » et le choix du
formalisme logique de l'assistant est alors important : nous voulons pouvoir
exprimer la preuve, et nous voulons le faire de manière simple et
intuitive. Notons que cela demande d'étudier le formalisme, notamment pour
des raisons de cohérence.

La correspondance de \bsc{Curry}-\bsc{Howard} et le $\lambda$-calcul typé
offrent un cadre parmi d'autres qui correspond à cela. L'idée est la suivante :
les preuves sont des objets, les propositions sont des types et une preuve de
la proposition $P$ et un objet $p$ de type $P$ (le type $P$) est habité.

Ainsi, une implication $P \to Q$ peut être vue comme une fonction du type
$P$ vers le type $Q$ (à partir d'une preuve de $P$, on construit la preuve
de $Q$). Ce point de vue qui rapproche la déduction et le calcul nous permet
de considérer une preuve d'un théorème comme un programme qui est la preuve
du théorème.

Par exemple, une preuve que si la distance de $A$ à $B$ est la même que
celle de $A$ à $C$, alors la distance de $A$ à $B$ est la même que celle
de $C$ à $A$ aurait ce type.

\begin{minted}{coq}
(A: Point) -> (B: Point) -> (C: Point) ->
(H: d(A, B) = d(A, C)) -> d(A, B) = d(C, A).
\end{minted}
Un élément de ce type est une fonction qui prend trois points et un élément
\verb|H| du type \verb|d(A, B) = d(A, C)| (donc une preuve) et retourne un
élément du type \verb|d(A, B) = d(C, A)|. On peut également également ce type
avec une implication.

\begin{minted}{coq}
(A: Point) -> (B: Point) -> (C: Point) ->
d(A, B) = d(A, C) => d(A, B) = d(C, A).
\end{minted}
Un élément de ce typr est une fonction qui prend en paramètre trois points et
retourne un élément du type \verb|d(A, B) = d(A, C) => d(A, B) = d(C, A)|, donc
une preuve que l'implication est vraie.

%(ici, on voit alors l'intérêt
%d'identifier une preuve de l'implication $P \to Q$ à une fonction qui
%construit une preuve de $Q$ avec une preuve de $P$).

Cette approche se base sur du $\lambda$-calcul typé et il nous faut décider
quelles abstractions sont autorisés ce qui offre plusieurs formalismes et
plusieurs fonctionnalités. En $\lambda$-calcul simplement typé, la seule
abstraction autorisée est celle qui permet de créer des termes qui dépendent
de terme, par exemple, une fonction qui prend un point et retourne un
point. Mais on peut par exemple autoriser un type à dépendre d'un
type ! Ces différentes fonctionnalités sont représentés sur le $\lambda$-cube en \cref{fig-lambda-cube}.

\begin{figure}[h]
    \centering
\begin{tikzpicture}
    \matrix (m) [matrix of math nodes,
    row sep=2em, column sep=2em,
    text height=1.5ex,
    text depth=0.25ex]{
        & \lambda\omega             &              & \CoC             \\
        \lambda 2   &                           & \lambda\Pi 2                                \\
        & \lambda\underline{\omega} &              & \lambda\Pi\underline{\omega} \\
        \lambda{\to}&                           & \lambda\Pi  \\
    };
    \path[-{Latex[length=2.5mm, width=1.5mm]}]
    (m-1-2) edge (m-1-4)
    (m-2-1) edge (m-2-3)
    edge (m-1-2)
    (m-3-2) edge (m-1-2)
    edge (m-3-4)
    (m-4-1) edge (m-2-1)
    edge (m-3-2)
    edge (m-4-3)
    (m-3-4) edge (m-1-4)
    (m-2-3) edge (m-1-4)
    (m-4-3) edge (m-3-4)
    edge (m-2-3);
\end{tikzpicture}
\caption{Le lambda-cube.}
\label{fig-lambda-cube}
\end{figure}

\begin{itemize}
\item Sur l'axe des $y$, on a les termes qui peuvent dépendre de type,
c'est-à-dire du \emph{polymorphisme} : par exemple, l'identité est
polymorphe, elle peut s'appliquer à un élément de n'importe quel type,
donc est du type \verb|forall A, A -> A|.
\item Sur l'axe des $x$, on a les types qui peuvent dépendre de types,
donc des \emph{opérateurs de types} : par exemple, \verb|pair| qui
aurait le type \verb|Type -> Type| et donc \verb|pair Point| serait
un \verb|Type|, celui des paires de points.
\item Sur l'axe des $z$, on a les types qui peuvent dépendre de termes ;
ce sont des \emph{types dépendants} : par exemple, \verb|vector| aurait
le type \verb|nat -> Type| et donc \verb|vector 4| serait un \verb|Type|,
celui des vecteurs à 4 éléments.
\end{itemize}
En autorisant toutes les abstractions, on obtient $\CoC$, le Calcul des Constructions, où on a les types suivants.
\begin{itemize}
    \item Les termes ont leur type (par exemple on a des types \verb|Point|,
          \verb|Prop|, etc.).
    \item Puisqu'on utilise les types comme des objets, ils ont également un
          type qui est \verb|Type|. Ainsi, \verb|Point| a le type \verb|Type|.
    \item On peut abstraire sur des types ; on donne alors à \verb|Type|
          le type \verb|Kind| car pour des raisons de cohérence, il ne peut
          pas avoir le type \verb|Type| ( \cf{} \cite{Girard, Coquand}).
\end{itemize}
Plus on autorise des fonctionnalités, plus on obtient un langage expressif.
De plus, ces fonctionnalités semblent être utiles pour faire des preuves de
manière simple et intuitive. Par exemple, nous voulons faire des preuves
avec des polynômes d'un certain degré (types dépendants) ou sur des
espaces vectoriels quelconques (opérateurs de types).

Pourtant, les assistants de preuve n'offrent pas tous ces
fonctionnalités (par exemple, les systèmes basés sur HOL n'ont pas de types
dépendants). Ceci rend compliqué le partage de preuves, alors que c'est
quelque chose que nous devrions chercher pour plusieurs raisons.
\begin{itemize}
    \item Gain de temps : ne pas refaire les preuves dans tous les systèmes.
    \item Gain de sûreté : car la preuve est vérifiée dans plusieurs
          systèmes.
    \item Gain de « popularité » : notamment auprès des mathématiciens.
\end{itemize}

\subsection{L'assistant de preuve Coq}

L'assistant de preuves Coq se base sur le calcul des constructions
auquel il rajoute quelques éléments.

\subsubsection{Les types inductifs}
    Nous pouvons créer le type des entiers naturels, ou encore un type
    des listes (qui mélange même type inductif et polymorphisme).
    \begin{minted}{coq}
Inductive list (A : Type) :=
| nil : list A
| cons : A -> list A -> list A.
    \end{minted}

\subsubsection{Les univers}
    En Coq, $0$ a le type \verb|nat|, qui a le type \verb|Set|, qui
    lui-même a le type $\Type$. Mais Coq permet d'abstraire sur
    n'importe quel type d'objets et nous pouvons écrire une fonction
    qui prend en paramètre un objet de type $\Kind$. Pour des raisons de
    cohérence là-encore, cette fonction ne peut pas être de type $\Kind$ et
    il nous faut un nouveau type avec lequel on fait la même chose et ainsi
    de suite...

    Ceci mène à une hiérarchie de types (appelés \emph{univers} en Coq).
    $\Type_0$ correspond à notre $\Type$ « classique » (le type de \verb|Set|),
    $\Type_1$ au type de $\Type_0$, etc. On a alors deux règles pour les
    univers de Coq.
    \begin{align*}
        \forall i, \Type_i \in \Type_{i + 1}.\\
        \forall i < j, \Type_i \in \Type_j.
    \end{align*}
    Ainsi, $\Type_i$, et tout terme de type $\Type_{i + 1}$a le type
    $\Type_j$ pour tout $j > i$ et n'a pas seulement le type $\Type_{i + 1}$. Il s'agit d'univers cumulatifs.

\subsection{Les mathématiques inversées et le partage de preuves}

Les \emph{mathématiques inversées} sont au cœur de mon travail et cette
expression apparaît d'ailleurs dans le titre de ce mémoire. Il s'agit
de prendre un théorème, et de minimiser les concepts mathématiques
nécessaires pour l'exprimer. Ce faisant, nous l'obtenons
dans une théorie plus petite.

Dans notre cas, nous regardons non seulement le théorème, mais aussi sa
preuve. Et s'ils ne nécessitent pas par exemple le principe du
tiers exclu, alors ce dernier être retiré de la théorie dans laquelle on exprime
notre théorème !

D'un point de vue théorique, connaître la théorie minimale dans laquelle
on peut exprimer un théorème est très intéressant. Dans le cadre du partage
de preuves, nous cherchons à connaître la théorie minimale pour pouvoir
ensuite exporter la preuve facilement dans d'autres systèmes.

En effet, on voudrait se passer des fonctionnalités qui ne sont pas présentes
dans certains systèmes (par exemple les types dépendants). En partant d'une
preuve en Coq, il nous faudra donc également chercher à
supprimer ce qui touche aux univers et aux types inductifs.

\begin{Attention}
    Une théorie peut ne pas disposer d'une fonctionnalité,
    mais l'utiliser au niveau basique de ses axiomes.
    Par exemple, le quantificateur universel prend en paramètre un
    type $T$ et un prédicat de $T$, donc semble nécessiter du polymorphisme.
    Mais ce n'est pas le cas, nous pouvons tout à fait autoriser le
    quantificateur universel dans une théorie tout en n'autorisant pas le
    polymorphisme.

    En fait, nous pouvons nous dire que le quantificateur universel est défini par un \emph{schéma d'axiomes} s'appliquant à un type, ce schéma étant
    défini dans la méta-théorie et pas dans la théorie.
\end{Attention}

%Ainsi, lors du partage de preuves entre deux systèmes, nous devons également
%faire un passage des briques élémentaires du premier à celle du deuxième.